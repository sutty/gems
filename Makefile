SHELL := bash
.DEFAULT_GOAL := help

help: always ## Ayuda
	@grep "^[^\t]\+:.*##" Makefile | sed -re "s/(.*):.*##(.*)/\1;\2/" | column -s ";" -t | sed -re "s/^([^ ]+) /\x1B[38;5;197m\1\x1B[0m/"

%: ## Agrega la gema a la cola de compilación (make puma v=5.3.0)
	@test -n "$v" || echo "Falta la versión en el argumento v=" ; :
	@test -n "$v" || exit 1
	install -dm 755 build/$@
	touch build/$@/$(v)
	git add build/$@/$(v)

missing:
	@comm -23 <(find build/ -type f | grep -v /.keep | cut -d / -f 2- | sort) <(find built/ -type f | grep -v /.keep | cut -d / -f 3- | sort)

.PHONY: always
