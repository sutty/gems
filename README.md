# Compilar gemas

Algunas gemas se vinculan con librerías binarias.  Para ahorrarnos los
recursos y acelerar los tiempos de instalación, pre-compilamos las gemas
usando gem-compiler y las distribuimos en nuestro propio repositorio de
gemas: <https://gems.sutty.nl/>


## Instalación

Es necesario generar los siguientes contenedores:

* [sutty/monit](https://0xacab.org/sutty/containers/monit)
* [sutty/sdk](https://0xacab.org/sutty/containers/sdk)
* [sutty/sdk-ruby](https://0xacab.org/sutty/containers/sdk-ruby)
* [sutty/gem-compiler](https://0xacab.org/sutty/containers/gem-compiler)


## Compilación

Para compilar una gema, correr el comando `make GEMA v=VERSION` donde
`GEMA` es el nombre y `VERSION` la versión, por ejemplo:

```bash
make puma v=5.3.1
```

Compila la gema puma versión 5.3.1 y la sube al repositorio de gemas.
